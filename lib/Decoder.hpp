﻿/*!
 *\file Decoder.hpp
 *\brief Файл с функциями для декодирования текста, закодированного циклическим
 * сдвигом.
 */

#ifndef DECODER_H
#define DECODER_H

#include <QVector>
#include <QStringList>
#include <QMap>

/*! Перечисление знаков препинания. */
enum class Symbols
{
    SPACE = 0,
    POINT = 27,
    COMMA,
    DASH,
    LF,
};

/*!
 * Уменьшить каждый символ входной последовательности на один.
 *
 *\param[in|out] Входной изменяемый текст.
 */
void next_variant(QVector<int>& text);

/*!
 * Преобразовать список строк в список последовательностей кодированых символов.
 *
 *\param[in] text Входной текст.
 *\param[out] symbols Список последовательностей символов в своей кодировке.
 */
void convert_to_array(const QStringList& text, QVector<int>& symbols);

/*!
 * Преобразовать последовательность кодированых символов в строку.
 *
 *\param[in] symbols Последовательность символов в своей кодировке.
 *\param[out] text Раскодированный текст.
 */
void convert_to_string(const QVector<int>& symbols, QStringList& text); 

/*!
 * Определить наличие вхождения слова в текст.
 *
 *\param[in] text Текст, в котором ищется слово.
 *\param[in] word Слово.
 *\return Признак наличия вхождения слова в текст.
 */
bool contain_word(
    const QVector<int>& text,
    const QVector<int>& word);

/*!
 * Определить наличие вхождения слова word в текст text, начиная с индекса
 * start_index.
 *
 *\details Дополнительно функция подсчитывает количество символов 29 и 30 в
 * тексте внутри слова.
 *
 *\param[in] text Входной текст.
 *\param[in] word Искомое слово.
 *\param[in] start_index Индекс в тексте, начиная с которого, нужно проверить
 * наличие слова.
 *\param[in] amount_of_hyphens Количество символов 29 и 30 внутри слова.
 */
bool contain_word_from_index(
    const QVector<int>& text,
    const QVector<int>& word,
    int start_index,
    int& amount_of_hyphens);

/*!
 * Проверить границы найденного слова в тексте.
 *
 *\details Результат будет истинным только в случае, если указанные индексы
 * являются началом или концом текста, или значение по индексу не является
 * буквой английского текста.
 *
 *\param[in] text Входной текст.
 *\param[in] start_index Начальный индекс найденного слова в тексте.
 *\param[in] end_index Конечный индекс найденного слова в тексте.
 */
bool check_border(const QVector<int>& text, int start_index, int end_index);

/*!
 * Преобразовать закодированные строки в набор декодированных вариантов.
 *
 *\param[in] text Текст в закодированном формате.
 *\param[in] word Достоверное слово.
 *\param[out] decoded_phrases Набор декодированных строк.
 *\return Количество вариантов декодировки.
 */
int decode(
    const QString& text,
    const QString& word,
    QMap<int, QString>& decoded_phrases
);

#endif // DECODER_H
