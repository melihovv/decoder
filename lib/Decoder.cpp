#include "Decoder.hpp"
#include <QSet>

/*! Коды символов, не являющихся английским буквами. */
static QSet<int> dividers{0, 27, 28, 29, 30};

void next_variant(QVector<int>& text)
{
    // По каждому закодированному символу...
    for (int& symbol : text)
    {
        // Пробел становится переводом строки.
        if (symbol == 0)
        {
            symbol = 30;
        }
        // Все остальные символы сдвигаются влево на один символ.
        else if (symbol > 0 && symbol <= 30)
        {
            symbol = symbol - 1;
        }
        else
        {
            throw std::exception("Invalid symbol");
        }
    }
}

void convert_to_array(const QStringList& text, QVector<int>& symbols)
{
    int line_number = 1;
    int column_number = 1;
    int counter = 1;

    // По каждой строке текста...
    for (const QString& string : text)
    {
        // По каждому символу строки...
        for (const QChar& c : string)
        {
            // Конвертируем каждый символ строки в его закодированный вид.

            if (c.isLower())
            {
                symbols << c.toLatin1() - 96;
            }
            else if (c == ' ')
            {
                symbols << (int) Symbols::SPACE;
            }
            else if (c == '.')
            {
                symbols << (int) Symbols::POINT;
            }
            else if (c == ',')
            {
                symbols << (int) Symbols::COMMA;
            }
            else if (c == '-')
            {
                symbols << (int) Symbols::DASH;
            }
            else if (c == '-')
            {
                column_number = 1;
                ++line_number;
            }
            else
            {
                throw std::exception(QString("Symbol %1 in string %2 at column %3 is not acceptable for this program")
                    .arg(c)
                    .arg(line_number)
                    .arg(column_number)
                    .toStdString()
                    .c_str());
            }

            ++counter;
            ++column_number;
        }

        // Не забываем добавить символ перевода строки после каждой строчки.
        symbols << (int) Symbols::LF;
    }

    // Удаляем последний лишний перевод строки.
    symbols.removeLast();
}

void convert_to_string(const QVector<int>& symbols, QStringList& text)
{
    QString buffer;

    // По каждому закодированному символу...
    for (int symbol : symbols)
    {
        // Добавляем в буфер раскодированный символ.

        if (symbol >= 1 && symbol <= 26)
        {
            buffer += QChar::fromLatin1(symbol + 96);
        }
        else
        {
            switch (symbol)
            {
                case (int) Symbols::SPACE:
                    buffer += ' ';
                    break;
                case (int) Symbols::POINT:
                    buffer += '.';
                    break;
                case (int) Symbols::COMMA:
                    buffer += ',';
                    break;
                case (int) Symbols::DASH:
                    buffer += '-';
                    break;
                case (int) Symbols::LF:
                    // Добавляем строку в выходной текст и очищаем буфер для
                    // следующей строки.

                    buffer += '\n';
                    text << buffer;
                    buffer.clear();
                    break;
                default:
                    throw std::exception("Invalid symbol");
            }
        }
    }

    // Если последняя строка не оканчивается переводом строки, то также добавить
    // ее.
    if (!buffer.isEmpty())
    {
        text << buffer;
    }
}

bool contain_word(const QVector<int>& text, const QVector<int>& word)
{
    int word_size = word.size();

    // По каждому символу текста...
    for (int i = 0, size = text.size(); i < size; ++i)
    {
        int amount_of_hyphens = 0;
        bool result = contain_word_from_index(text, word, i, amount_of_hyphens);

        if (result &&
            check_border(text, i, i + word_size + amount_of_hyphens - 1))
        {
            return true;
        }
    }

    return false;
}

bool contain_word_from_index(
    const QVector<int>& text,
    const QVector<int>& word,
    int start_index,
    int& amount_of_hyphens /*= 0*/)
{
    int size = word.size();
    int length = text.size();
    bool result = true;

    // Если индекс слишком большой, чтобы после него поместилось слово.
    if (start_index + size > text.size())
    {
        return false;
    }

    // По каждому символу слова, пока не встретили несоответствие.
    for (int i = 0; i < size && result; ++i)
    {
        result &= word[i] == text[start_index++];

        // Если индекс не последний в слове.
        if (i + 1 < size)
        {
            // Если следующий символ слова дефис (29)...
            if (word[i + 1] == 29)
            {
                // Если следующий индекс текста перевод строки (30)...
                if (start_index < length && text[start_index + 1] == 30)
                {
                    // Перемещаем индекс текста на два (через дефис и перевод
                    // строки).
                    start_index += 2;
                    // Перемещаем индекс слова на один (через дефис).
                    ++i;
                    // Увеличиваем количество переносов на один.
                    ++amount_of_hyphens;
                }
            }
            else
            {
                // Если следующие символы текста дефис и перевод строки...
                if (start_index < length && start_index + 1 < length &&
                    text[start_index] == 29 && text[start_index + 1] == 30)
                {
                    // Перемещаем индекс текста на два (через дефис и перевод
                    // строки).
                    start_index += 2;
                    // Увеличиваем количество переносов на два.
                    amount_of_hyphens += 2;
                }
            }
        }
    }

    return result;
}

bool check_border(const QVector<int>& text, int start_index, int end_index)
{
    // Если индекс начального символа начало текста, или если он не является
    // буквой, и если индекс конечного символа слова конец текста, или если он
    // не является буквой.
    return
        (start_index == 0 || dividers.contains(text[start_index - 1])) &&
        (end_index == text.size() - 1 || dividers.contains(text[end_index + 1]));
}

int decode(const QString& text,
    const QString& word,
    QMap<int, QString>& decode_phrases)
{
    // Конвертируем текст и слово в их кодированные варианты.

    QVector<int> input_array;
    convert_to_array(text.split("\n"), input_array);

    QVector<int> word_array;
    convert_to_array(QStringList{word}, word_array);

    int number_of_variants = 0;

    // Пробуем все возможные сдвиги: от 0 до 30.
    for (int shift_number = 0; shift_number <= 30; ++shift_number)
    {
        // Если текст содержит слово.
        if (contain_word(input_array, word_array))
        {
            ++number_of_variants;

            // Декодируем текст.
            QStringList decoded_text;
            convert_to_string(
                input_array,
                decoded_text
            );

            // И вставляем в результирующую карту.
            decode_phrases.insert(shift_number, decoded_text.join(""));
        }

        next_variant(input_array);
    }

    return number_of_variants;
}
