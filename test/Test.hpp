#ifndef TEST_HPP
#define TEST_HPP

#include <QObject>
#include <QTest>
#include <vendor/TestRunner.hpp>

typedef QMap<int, QString> Phrases;

class Test : public QObject
{
    Q_OBJECT

private:
    static QString vector_to_string(const QVector<int>& list);
    static QString map_to_string(const Phrases& map);

private slots:
    void test_next_variant_data();
    void test_next_variant();

    void test_convert_to_array_data();
    void test_convert_to_array();

    void test_convert_to_string_data();
    void test_convert_to_string();

    void test_contain_word_data();
    void test_contain_word();

    void test_decode_data();
    void test_decode();
    
    void test_check_border_data();
    void test_check_border();

    void test_contain_word_from_index_data();
    void test_contain_word_from_index();
};

DECLARE_TEST(Test);

#endif // TEST_HPP
