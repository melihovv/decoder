#include "Test.hpp"
#include <lib/Decoder.hpp>

QString Test::vector_to_string(const QVector<int>& list)
{
    QString result;
    int length = list.length();

    for (int i = 0; i < length - 1; ++i)
    {
        result.append(QString::number(i) + ", ");
    }

    if (length > 1)
    {
        result.append(QString::number(list[length - 1]));
    }

    return result;
}

QString Test::map_to_string(const Phrases& map)
{
    QString result;
    int length = map.size();

    QStringList temp;
    for (const auto& phrase : map)
    {
        temp << phrase;
    }

    return temp.join(', ');
}

void Test::test_next_variant_data()
{
    QTest::addColumn<QVector<int>>("input");
    QTest::addColumn<QVector<int>>("expectation");
    QTest::addColumn<bool>("should_throw_exception");

    QTest::newRow("The text consists of all possible elements of an array")
        << QVector<int>{
            0, 1, 2, 3, 27, 0, 4, 5, 6, 7, 8, 9, 30, 10, 11, 12, 13, 14, 15,
            16, 28, 30, 17, 18, 19, 29, 20, 0, 29, 21, 22, 28, 0, 23, 24, 29,
            0, 25, 26, 27
        }
        << QVector<int>{
            30, 0, 1, 2, 26, 30, 3, 4, 5, 6, 7, 8, 29, 9, 10, 11, 12, 13, 14,
            15, 27, 29, 16, 17, 18, 28, 19, 30, 28, 20, 21, 27, 30, 22, 23, 28,
            30, 24, 25, 26
        }
        << false;

    QTest::newRow("Exception")
        << QVector<int>{-1}
        << QVector<int>{}
        << true;
}

void Test::test_next_variant()
{
    QFETCH(QVector<int>, input);
    QFETCH(QVector<int>, expectation);
    QFETCH(bool, should_throw_exception);

    if (!should_throw_exception)
    {
        next_variant(input);

        int input_length = input.length();
        int expectation_length = expectation.length();

        if (input_length != expectation_length)
        {
            QVERIFY2(
                input_length == expectation_length,
                QString("\nДлины списков не равны.\nОжидалось:\n%1\nНа самом деле:\n%2")
                .arg(Test::vector_to_string(expectation))
                .arg(Test::vector_to_string(input))
                .toLocal8Bit()
                .data()
            );
        }

        for (int i = 0; i < input_length; ++i)
        {
            QVERIFY2(
                expectation[i] == input[i],
                QString("\nСимвол по счету: %1\nОжидалось: %2\nНа самом деле: %3")
                .arg(QString::number(i + 1))
                .arg(QString::number(expectation[i]))
                .arg(QString::number(input[i]))
                .toLocal8Bit()
                .data()
            );
        }
    }
    else
    {
        try
        {
            next_variant(input);
            QVERIFY2(false, "next_variant doesn't throw exception");
        }
        catch (const std::exception& e)
        {
            QVERIFY(true);
        }
    }
}

void Test::test_convert_to_array_data()
{
    QTest::addColumn<QStringList>("input");
    QTest::addColumn<QVector<int>>("expectation");
    QTest::addColumn<bool>("should_throw_exception");

    QTest::newRow("The text consists of all possible elements of an array")
        << QStringList{" abc. defghi", "jklmnop,", "qrs-t -uv, wx- yz.",}
        << QVector<int>{
            0, 1, 2, 3, 27, 0, 4, 5, 6, 7, 8, 9, 30,
            10, 11, 12, 13, 14, 15, 16, 28, 30,
            17, 18, 19, 29, 20, 0, 29, 21, 22, 28, 0, 23, 24, 29, 0, 25, 26, 27,
        }
        << false;

    QTest::newRow("Exception")
        << QStringList{"^"}
        << QVector<int>{}
        << true;
}

void Test::test_convert_to_array()
{
    QFETCH(QStringList, input);
    QFETCH(QVector<int>, expectation);
    QFETCH(bool, should_throw_exception);

    if (!should_throw_exception)
    {
        QVector<int> real;
        convert_to_array(input, real);

        int real_length = real.length();
        int expectation_length = expectation.length();

        if (real_length != expectation_length)
        {
            QVERIFY2(
                real == expectation,
                QString("\nДлины списков не равны.\nОжидалось:\n%1\nНа самом деле:\n%2")
                .arg(Test::vector_to_string(expectation))
                .arg(Test::vector_to_string(real))
                .toLocal8Bit()
                .data()
            );
        }

        for (int i = 0; i < real_length; ++i)
        {
            QVERIFY2(
                expectation[i] == real[i],
                QString("\nСимвол по счету: %1\nОжидалось: %2\nНа самом деле: %3")
                .arg(QString::number(i + 1))
                .arg(QString::number(expectation[i]))
                .arg(QString::number(real[i]))
                .toLocal8Bit()
                .data()
            );
        }
    }
    else
    {
        try
        {
            QVector<int> symbols;
            convert_to_array(input, symbols);
            QVERIFY2(false, "convert_to_array doesn't throw exception");
        }
        catch (const std::exception& e)
        {
            QVERIFY(true);
        }
    }
}

void Test::test_convert_to_string_data()
{
    QTest::addColumn<QVector<int>>("input");
    QTest::addColumn<QStringList>("expectation");
    QTest::addColumn<bool>("should_throw_exception");

    QTest::newRow("The text consists of all possible elements of an array")
        << QVector<int>{
            0, 1, 2, 3, 27, 0, 4, 5, 6, 7, 8, 9, 30,
            10, 11, 12, 13, 14, 15, 16, 28, 30,
            17, 18, 19, 29, 20, 0, 29, 21, 22, 28, 0, 23, 24, 29, 0, 25, 26, 27,
        }
        << QStringList{" abc. defghi\n", "jklmnop,\n", "qrs-t -uv, wx- yz.",}
        << false;

    QTest::newRow("Exception")
        << QVector<int>{-1}
        << QStringList{}
        << true;
}

void Test::test_convert_to_string()
{
    QFETCH(QVector<int>, input);
    QFETCH(QStringList, expectation);
    QFETCH(bool, should_throw_exception);

    if (!should_throw_exception)
    {
        QStringList real;
        convert_to_string(input, real);

        int real_length = real.length();
        int expectation_length = expectation.length();

        if (real_length != expectation_length)
        {
            QVERIFY2(
                real == expectation,
                QString("\nДлины списков не равны.\nОжидалось:\n%1\nНа самом деле:\n%2")
                .arg(expectation.join(', '))
                .arg(real.join(', '))
                .toLocal8Bit()
                .data()
            );
        }

        for (int i = 0; i < real_length; ++i)
        {
            bool flag = expectation[i] == real[i];
            QVERIFY2(
                expectation[i] == real[i],
                QString("\nСимвол по счету: %1\nОжидалось: %2\nНа самом деле: %3")
                .arg(QString::number(i + 1))
                .arg(expectation[i])
                .arg(real[i])
                .toLocal8Bit()
                .data()
            );
        }
    }
    else
    {
        try
        {
            QStringList text;
            convert_to_string(input, text);
            QVERIFY2(false, "convert_to_string doesn't throw exception");
        }
        catch (const std::exception& e)
        {
            QVERIFY(true);
        }
    }
}

void Test::test_contain_word_data()
{
    QTest::addColumn<QVector<int>>("text");
    QTest::addColumn<QVector<int>>("word");
    QTest::addColumn<bool>("expectation");

    QTest::newRow("The text consists of only one word")
        << QVector<int>{
            1, 2, 3, 4,
        }
        << QVector<int>{
            1, 2, 3, 4,
        }
        << true;

    QTest::newRow("The text contains the word separated by spaces")
        << QVector<int>{
            0, 2, 7, 0,
        }
        << QVector<int>{
            2, 7,
        }
        << true;

    QTest::newRow("The text doesn't contain the word")
        << QVector<int>{
            0, 2, 22, 0,
        }
        << QVector<int>{
            2, 7,
        }
        << false;

    QTest::newRow("The word is the beginning of another longer word, standing at the beginning of the text")
        << QVector<int>{
            1, 2, 3, 4, 0, 1,
        }
        << QVector<int>{
            1, 2,
        }
        << false;

    QTest::newRow("The word is the beginning of another longer word, standing in the middle of the text")
        << QVector<int>{
            1, 0, 1, 2, 3, 4, 0, 1,
        }
        << QVector<int>{
            1, 2,
        }
        << false;

    QTest::newRow("The word is the beginning of another longer word, standing at the end of the text")
        << QVector<int>{
            1, 2, 2, 3, 1, 0, 9,
        }
        << QVector<int>{
            2, 3,
        }
        << false;

    QTest::newRow("The word is in the middle of another longer word, standing at the beginning of the text")
        << QVector<int>{
            1, 2, 2, 3, 1, 0, 9,
        }
        << QVector<int>{
            2, 3,
        }
        << false;

    QTest::newRow("The word is in the middle of another longer word, standing in the middle of the text")
        << QVector<int>{
            1, 0, 1, 2, 2, 3, 1, 0, 9,
        }
        << QVector<int>{
            2, 3,
        }
        << false;

    QTest::newRow("The word is in the middle of another longer word, standing at the end of the text")
        << QVector<int>{
            1, 0, 1, 2, 2, 3, 1,
        }
        << QVector<int>{
            2, 3,
        }
        << false;

    QTest::newRow("The word is at the end of another longer word, standing at the beginning of the text")
        << QVector<int>{
            1, 2, 3, 4, 0, 1,
        }
        << QVector<int>{
            3, 4,
        }
        << false;

    QTest::newRow("The word is at the end of another longer word, standing in the middle of the text")
        << QVector<int>{
            1, 0, 1, 2, 3, 4, 0, 1,
        }
        << QVector<int>{
            3, 4,
        }
        << false;

    QTest::newRow("The word is at the end of another longer word, standing at the end of the text")
        << QVector<int>{
            1, 2, 3, 4, 0, 1,
        }
        << QVector<int>{
            3, 4,
        }
        << false;

    QTest::newRow("The word is at the beginning of the text")
        << QVector<int>{
            1, 2, 3, 0, 1,
        }
        << QVector<int>{
            1, 2, 3,
        }
        << true;

    QTest::newRow("The word is in the middle of the text")
        << QVector<int>{
            1, 0, 1, 2, 3, 0, 2,
        }
        << QVector<int>{
            1, 2, 3,
        }
        << true;

    QTest::newRow("The word is at the end of the text")
        << QVector<int>{
            1, 0, 1, 2, 3,
        }
        << QVector<int>{
            1, 2, 3,
        }
        << true;

    QTest::newRow("The word appears twice in the text")
        << QVector<int>{
            1, 0, 1, 2, 3, 0, 1, 2, 3,
        }
        << QVector<int>{
            1, 2, 3,
        }
        << true;

    QTest::newRow("The word appears twice in the text, wherein once as a part of another word")
        << QVector<int>{
            1, 0, 1, 2, 3, 4, 0, 1, 2, 3,
        }
        << QVector<int>{
            1, 2, 3,
        }
        << true;

    QTest::newRow("The word is not in the text, but there is its part")
        << QVector<int>{
            1, 0, 1, 2,
        }
        << QVector<int>{
            1, 2, 3,
        }
        << false;

    QTest::newRow("The text consists of only spaces")
        << QVector<int>{
            0, 0, 0, 0,
        }
        << QVector<int>{
            1, 2, 3,
        }
        << false;

    QTest::newRow("The text is empty")
        << QVector<int>{
        }
        << QVector<int>{
            1, 2, 3,
        }
        << false;

    QTest::newRow("The word consists of only one letter")
        << QVector<int>{
            1, 0, 1, 2,
        }
        << QVector<int>{
            1,
        }
        << true;

    QTest::newRow("The word contains hyphen, which is not a sign of hyphenation")
        << QVector<int>{
            5, 0, 2, 1, 29, 1, 2, 0,
        }
        << QVector<int>{
            2, 1, 29, 1, 2,
        }
        << true;

    QTest::newRow("The text contains line breaks")
        << QVector<int>{
            2, 30, 0, 2, 30, 1, 0, 5, 1, 3,
        }
        << QVector<int>{
            5, 1, 3,
        }
        << true;

    QTest::newRow("The text contains punctuation marks")
        << QVector<int>{
            4, 27, 6, 4, 28, 7, 7, 29, 1, 3, 5,
        }
        << QVector<int>{
            7, 7,
        }
        << true;

    QTest::newRow("The text contains punctuation marks and line breaks")
        << QVector<int>{
            4, 28, 6, 4, 29, 7, 7, 30, 1, 3, 0, 5,
        }
        << QVector<int>{
            7, 7,
        }
        << true;

    QTest::newRow("The word is hyphenated")
        << QVector<int>{
            1, 2, 29, 30, 1, 2, 3, 0, 1,
        }
        << QVector<int>{
            1, 2, 1, 2, 3,
        }
        << true;

    QTest::newRow("The word is hyphenated on hyphenation mark")
        << QVector<int>{
            1, 2, 29, 30, 1, 2, 3, 0, 1,
        }
        << QVector<int>{
            1, 2, 29, 1, 2, 3,
        }
        << true;
}

void Test::test_contain_word()
{
    QFETCH(QVector<int>, text);
    QFETCH(QVector<int>, word);
    QFETCH(bool, expectation);

    bool real = contain_word(text, word);

    QString message = QString("\nОжидалось: %1\nНа самом деле: %2");

    QVERIFY2(
        expectation == real,
        message
            .arg(expectation ? "true" : "false")
            .arg(real ? "true" : "false")
            .toLocal8Bit()
            .data()
    );
}

void Test::test_decode_data()
{
    QTest::addColumn<QString>("input");
    QTest::addColumn<QString>("true_word");
    QTest::addColumn<Phrases>("decode_phrases");
    QTest::addColumn<int>("number_of_variants");

    QTest::newRow("The text consists of only one non-encrypted word")
        << "abcd"
        << "abcd"
        << Phrases{{0, "abcd"},}
        << 1;

    QTest::newRow("The text doesn't contain the word")
        << "abab"
        << "baba"
        << Phrases{}
        << 0;

    QTest::newRow("The text consists of only one encrypted word")
        << "cdef"
        << "abcd"
        << Phrases{{2, "abcd"}}
        << 1;

    QTest::newRow("The text consists of not only one encrypted word")
        << "ifmmpajabnajhps"
        << "am"
        << Phrases{{1, "hello i am igor"}}
        << 1;

    QTest::newRow("The text consists of only spaces or punctuation marks")
        << "    ..,,--\n"
        << "abc"
        << Phrases{}
        << 0;

    QTest::newRow("The text is empty")
        << ""
        << "abc"
        << Phrases{}
        << 0;

    QTest::newRow("The word is hyphenated")
        << "ifm\n mpajabnajhps"
        << "hello"
        << Phrases{{1, "hel-\nlo i am igor"}}
        << 1;

    QTest::newRow("The word has hyphen")
        << "if\nmmpajabnajhps"
        << "he-llo"
        << Phrases{{1, "he-llo i am igor"}}
        << 1;

    QTest::newRow("There are two variants of translation")
        << "epha,ald"
        << "dog"
        << Phrases{{1, "dog . kc"}, {28, "hskd dog"}}
        << 2;
}

void Test::test_decode()
{
    QFETCH(QString, input);
    QFETCH(QString, true_word);
    QFETCH(Phrases, decode_phrases);
    QFETCH(int, number_of_variants);

    Phrases real_decode_phrases;
    int real_number_of_variants =
        decode(input, true_word, real_decode_phrases);

    QString message = QString("\nКоличество вариантов декодировки не совпадает.\nОжидалось: %1\nНа самом деле: %2");

    QVERIFY2(
        number_of_variants == real_number_of_variants,
        message
            .arg(QString::number(number_of_variants))
            .arg(QString::number(real_number_of_variants))
            .toLocal8Bit()
            .data()
    );

    int real_length = real_decode_phrases.size();
    int expectation_length = decode_phrases.size();

    if (real_length != expectation_length)
    {
        QVERIFY2(
            real_length == expectation_length,
            QString("\nДлины наборов декодированных строк не равны.\nОжидалось:\n%1\nНа самом деле:\n%2")
                .arg(Test::map_to_string(decode_phrases))
                .arg(Test::map_to_string(real_decode_phrases))
                .toLocal8Bit()
                .data()
        );
    }

    foreach (int key, real_decode_phrases.keys())
    {
        QVERIFY2(
            decode_phrases.contains(key),
            QString("\nОжидаемые декодированные наборы выриантов не содержат ключа %1")
                .arg(QString::number(key))
                .toLocal8Bit()
                .data()
        );
    }

    foreach (int key, decode_phrases.keys())
    {
        QVERIFY2(
            real_decode_phrases.contains(key),
            QString("\nПолученные декодированные наборы выриантов не содержат ключа %1")
                .arg(QString::number(key))
                .toLocal8Bit()
                .data()
        );

        QVERIFY2(
            decode_phrases.value(key) == real_decode_phrases.value(key),
            QString("\nЗначения по ключу %1 не совпадают\nОжидалось: %2\nНа самом деле: %3")
                .arg(QString::number(key))
                .arg(decode_phrases.value(key))
                .arg(real_decode_phrases.value(key))
                .toLocal8Bit()
                .data()
        );
    }
}

void Test::test_check_border_data()
{
    QTest::addColumn<QVector<int>>("text");
    QTest::addColumn<int>("start_index");
    QTest::addColumn<int>("end_index");
    QTest::addColumn<bool>("expectation");

    QTest::newRow("Start and end indexes are beginning and end of the text respectively")
        << QVector<int>{1, 2, 3}
        << 0
        << 2
        << true;

    QTest::newRow("Start index is beginning of the text, end index is not letter")
        << QVector<int>{1, 2, 27}
        << 0
        << 2
        << true;

    QTest::newRow("Start index is not letter, end index is not letter")
        << QVector<int>{0, 1, 2, 27}
        << 1
        << 3
        << true;

    QTest::newRow("Start index is beggining of the text, end index is letter")
        << QVector<int>{0, 1, 2, 3, 0}
        << 1
        << 3
        << true;

    QTest::newRow("Start index is letter, end index is letter")
        << QVector<int>{1, 2, 3}
        << 0
        << 2
        << true;
}

void Test::test_check_border()
{
    QFETCH(QVector<int>, text);
    QFETCH(int, start_index);
    QFETCH(int, end_index);
    QFETCH(bool, expectation);

    int real = check_border(text, start_index, end_index);

    QString message =
        QString("\nРезультаты не совпадают.\nОжидалось: %1\nНа самом деле: %2");

    QVERIFY2(
        expectation == real,
        message
            .arg(expectation)
            .arg(real)
            .toLocal8Bit()
            .data()
    );
}

void Test::test_contain_word_from_index_data()
{
    QTest::addColumn<QVector<int>>("text");
    QTest::addColumn<QVector<int>>("word");
    QTest::addColumn<int>("start_index");
    QTest::addColumn<int>("amount_of_hyphens");
    QTest::addColumn<bool>("expectation");

    QTest::newRow("Text contains word")
        << QVector<int>{1, 2, 3}
        << QVector<int>{1, 2, 3}
        << 0
        << 0
        << true;

    QTest::newRow("Start index is too big")
        << QVector<int>{1, 2, 3}
        << QVector<int>{1, 2, 3}
        << 1
        << 0
        << false;

    QTest::newRow("Text with hyphen in the word")
        << QVector<int>{1, 2, 29, 30, 3, 0}
        << QVector<int>{1, 2, 29, 3}
        << 0
        << 1
        << true;

    QTest::newRow("Text with hyphen in the word 2")
        << QVector<int>{1, 2, 29, 30, 3, 0}
        << QVector<int>{1, 2, 3}
        << 0
        << 2
        << true;
}

void Test::test_contain_word_from_index()
{
    QFETCH(QVector<int>, text);
    QFETCH(QVector<int>, word);
    QFETCH(int, start_index);
    QFETCH(int, amount_of_hyphens);
    QFETCH(bool, expectation);

    int local_amount_of_hyphens = 0;
    bool real = contain_word_from_index(
        text,
        word,
        start_index,
        local_amount_of_hyphens);

    QString message =
        QString("\nРезультаты не совпадают.\nОжидалось: %1\nНа самом деле: %2");

    QVERIFY2(
        expectation == real,
        message
            .arg(expectation)
            .arg(real)
            .toLocal8Bit()
            .data()
    );

    message =
        QString("\nКоличество переносов не совпадает.\nОжидалось: %1\nНа самом деле: %2");

    QVERIFY2(
        amount_of_hyphens == local_amount_of_hyphens,
        message
            .arg(amount_of_hyphens)
            .arg(local_amount_of_hyphens)
            .toLocal8Bit()
            .data()
    );
}
