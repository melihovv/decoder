#include "vendor/TestRunner.hpp"

int main(int argc, char* argv[])
{
    setlocale(LC_ALL, "Russian");
    return RUN_ALL_TESTS(argc, argv);
}
