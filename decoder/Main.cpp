#include "../lib/Decoder.hpp"
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>

int main(int argc, char* argv[])
{
    try
    {
        if (argc != 2)
        {
            QString message = "Invalid argument count\n";
            message += "Usage: decoder.exe decoded_text.txt";
            throw std::exception(message.toStdString().c_str());
        }

        QFile input_file{argv[1]};
        if (!input_file.open(QIODevice::ReadOnly))
        {
            QString message = "Couldn`t open file " + QString(argv[1]);
            throw std::exception(message.toStdString().c_str());
        }

        QTextStream input_stream(&input_file);
        QString content = input_stream.readAll();

        if (content.isEmpty())
        {
            QString message = "Empty file: " + QString(argv[1]);
            throw std::exception(message.toStdString().c_str());
        }

        content = content.replace("\r\n", "\n");
        QStringList l = content.split('\n');

        QString true_word = l.last();
        l.pop_back();
        content = l.join('\n');

        QMap<int, QString> decoded_content;
        int number_of_variants = decode(content, true_word, decoded_content);

        QString output = "Number of variants: " +
            QString::number(number_of_variants) + "\n";

        foreach (int shift, decoded_content.keys())
        {
            output += "Shift: " + QString::number(shift) + "\n";
            output += "Decoded text:\n" + decoded_content.value(shift) + "\n";
        }

        QString output_file_name = QFileInfo{argv[1]}.baseName() + ".output.txt";
        QFile output_file{output_file_name};

        if (!output_file.open(QIODevice::WriteOnly))
        {
            QString message = "Couldn`t open file " + output_file_name;
            throw std::exception(message.toStdString().c_str());
        }

        QTextStream out{&output_file};
        out << output;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return -1;
    }

    return 0;
}
